package sample.math;

public class Factorial {

    public long compute(long n) {
        if (n <= 0) {
            return 1;
        }
        return n * compute(n -1);
    }

}
