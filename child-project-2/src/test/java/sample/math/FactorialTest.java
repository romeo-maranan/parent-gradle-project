package sample.math;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;

@RunWith(BlockJUnit4ClassRunner.class)
public class FactorialTest {

    private Factorial factorial = new Factorial();

    @Test
    public void testShouldComputeFactorialProperly() {
        long result = factorial.compute(3);
        assertEquals(6, result);
    }

}
