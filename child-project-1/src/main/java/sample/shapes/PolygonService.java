package sample.shapes;

import static java.lang.Math.PI;
import static java.lang.Math.pow;

public class PolygonService {

    public double getArea(Polygon shape) {
        if (shape instanceof Circle) {
            return circleArea((Circle) shape);
        }
        if (shape instanceof Rectangle) {
            return rectangleArea((Rectangle) shape);
        }
        if (shape instanceof Triangle) {
            return triangleArea((Triangle) shape);
        }
        return 0;
    }

    private double triangleArea(Triangle shape) {
        return  0.5 * shape.getBase() * shape.getHeight();
    }

    private double rectangleArea(Rectangle shape) {
        return shape.getWidth() * shape.getHeight();
    }

    private double circleArea(Circle shape) {
        return PI * pow(shape.getRadius(), 2);
    }

}
