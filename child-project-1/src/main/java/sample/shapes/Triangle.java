package sample.shapes;

public class Triangle extends Polygon {

    private double height;
    private double base;

    public Triangle(double height, double base) {
        super(3);
        this.height = height;
        this.base = base;
    }

    public double getHeight() {
        return height;
    }

    public double getBase() {
        return base;
    }

}
