package sample.shapes;

public class Polygon {

    private double sides;

    public Polygon(int sides) {
        this.sides = sides;
    }

    public double getSides() {
        return sides;
    }

}
