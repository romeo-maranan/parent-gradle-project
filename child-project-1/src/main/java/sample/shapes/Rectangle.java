package sample.shapes;

public class Rectangle extends Polygon {

    private double height;
    private double width;

    public Rectangle(double height, double width) {
        super(4);
        this.height = height;
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public double getWidth() {
        return width;
    }

}
