package sample.shapes;

public class Circle extends Polygon {

    private double radius;

    public Circle(int radius) {
        super(0);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

}
