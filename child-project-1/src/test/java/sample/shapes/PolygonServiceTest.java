package sample.shapes;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import static java.lang.Math.PI;
import static java.lang.Math.pow;
import static org.junit.Assert.assertEquals;

@RunWith(BlockJUnit4ClassRunner.class)
public class PolygonServiceTest {

    private PolygonService polygonService = new PolygonService();

    @Test
    public void testShouldComputeCircleArea() {
        Circle circle = new Circle(10);
        double area = polygonService.getArea(circle);
        assertEquals(PI * pow(10, 2), area, 0.01);
    }

    @Test
    public void testShouldComputeRectangleArea() {
        Rectangle rectangle = new Rectangle(10, 4);
        double area = polygonService.getArea(rectangle);
        assertEquals(40, area, 0);
    }

    @Test
    public void testShouldComputeTriangleArea() {
        Triangle triangle = new Triangle(10, 4);
        double area = polygonService.getArea(triangle);
        assertEquals(20, area, 0);
    }

}
